# Add transparency to form radio button icons

If your form building software has custom icons for radio buttons, and those icons are round, but they are not transparent, here's a way you can fix them.

Your radio buttons, on a non-white background, might look like this:

![Opaque radio button icons](images/image-1.png "Opaque radio button icons")

## Add round borders

The simplest solution would be to correct the images. However, in software that's provided for you, that's often not an option. 

If you can add CSS to the form, though, you can correct it.

Let's say your radio buttons have src paths like this:

* /pic/icon/CheckedRadio.gif
* /pic/icon/NotCheckedRadio.gif

Then, add a CSS rule, like so, to your form:

```css
img[src$="CheckedRadio.gif"] {
    border-radius: 50%;
}
```

And you'll have beautifully transparent icons:

![Transparent radio buttons](images/image-2.png "Transparent radio buttons")


